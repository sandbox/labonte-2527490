<?php
// $id:$

/**
 * @file
 * This file contains the Edutags specific implementation of the \SearchResponse
 * interface.
 */

namespace publicplan\wss\edutags;

use publicplan\wss\base\SearchResponse;

class Response extends SearchResponse {
  protected $service = Service::NAME;

  protected function parseResponseData() {
    $this->raw = json_decode($this->body, TRUE);
    $this->data['count'] = count($this->raw);

    return $this;
  }

  protected function extractResults() {
    if (!isset($this->raw) || !is_array($this->raw)) {
      throw new \RuntimeException(
          t('Got strange data portion for an Edutags response.'));
    }

    return $this->raw;
  }

//  public function format($raw = null) {
//    // Force results to format themselves.
//    $this->getResults();
//
//    if ($e = $this->getError()) {
//      drupal_set_message($e, 'error');
//    }
//  }
//
//  public function getFormatted() {
//    if (empty($this->results)) {
//      return array();
//    }
//
//    $result = reset($this->results);
//    return $result->getFormatted();
//  }
//
//  public function getOutput() {
//    if (empty($this->results)) {
//      return '';
//    }
//
//    return array_map(
//        function ($r) {
//          return array(
//            'id' => $r['#id'],
//            'resource' => $r['#resource'],
//            'html' => $r->getOutput(),
//          );
//        },
//        $this->results);
//  }
}
