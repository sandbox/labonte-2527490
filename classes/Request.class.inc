<?php
// $id:$

/**
 * @file
 * This file contains Edutags' Request class.
 */

namespace publicplan\wss\edutags;

use publicplan\wss\base\SearchRequest;

class Request extends SearchRequest {
  const CACHE = 'cache_edutags';

  protected $service = Service::NAME;

  protected $method = self::METHOD__GET;
  
  protected $parameters = array();
  
  public function setResourceUrls(array $urls) {
    $param_urls = implode(',', array_map('urlencode', $urls));
    
    $uri = str_replace(
        array('%KEY', '%ID'), 
        array(Service::getSetting(Service::SETTING__ACCESS_TOKEN), $param_urls),
            Service::getSetting(Service::SETTING__DOMAIN) .
            Service::getSetting(Service::SETTING__RESOURCE_PATH));

    return $this->setUrl($uri);
  }
}
