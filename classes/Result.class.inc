<?php

// $id:$

/**
 * @file
 * This file contains Edutags specific implementation of Result class.
 */

namespace publicplan\wss\edutags;

use publicplan\wss\base\SearchResult;

class Result extends SearchResult {
  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Associative array representing the structure of the formatted array.
   *
   * @var array
   */
  protected $format_structure = array(
    // Foreign node ID:
    'id' => '@/nid',
    'title' => '@/title',
    'description' => '@/description',
    'thumbnail' => '@/screenshot',
    'resource' => '@/url',
    'tags' => '@/tags/[]',
    'comments' => '@/comment/[]',
    'rating' => '@/voting',
    'license' => '@/license',
  );

  protected $format_callbacks = array(
    'tags' => 'self::reformatTags',
    'comments' => 'self::reformatComments',
    'rating' => 'self::getRatingClass',
  );

  /**
   * Parse result data.
   *
   * @param mixed $raw_data
   *   Raw result data.
   *
   * @return \publicplan\wss\base\SearchResult
   *   Returns itself.
   */
  public function parse($raw_data = null) {
    $this->raw = $raw_data;

    // Extract data defined via $this->format_structure.
    foreach ($this->format_structure as $render_key => $data_path) {
      if (is_string($data_path) && strpos($data_path, '@') === 0) {
        $path = explode('/', $data_path);
        array_shift($path);
        $this->data[$render_key] = isset($this->format_callbacks[$render_key]) &&
            is_callable($this->format_callbacks[$render_key])
            ? call_user_func($this->format_callbacks[$render_key],
                $this->extract($path))
            : $this->extract($path);
      }
    }

    $this->data['rating_stars'] = round($this->data['rating'] / 20);

    return $this;
  }

  /**
   * Extract data portion from raw result data.
   *
   * @param array $path
   *   Path/Array keys to data portion specified.
   *
   * @return mixed
   *   Extracted data.
   */
  protected function extract($path) {
    $key = reset($path);
    if (isset($this->raw[$key])) {
      $value = $this->raw[$key];
      while ($key = next($path)) {
        if ($key === '[]') {
          break;
        }

        is_array($value) && array_key_exists($key, $value)
          ? $value = &$value[$key]
          : $value = '';
      }
    } else {
      $value = null;
    }

    if (is_array($value) && $key !== '[]') {
      $value = reset($value);
    }
    elseif (!is_array($value) && $key === '[]') {
      $value = empty($value) ? array() : array($value);
    }

    if (!isset($value)) {
      $value = $key === '[]' ? array() : '';
    }

    return $value;
  }

  /**
   * Re-formats tags.
   *
   * Input: array( array( ID1, TAG1, COUNT1 ), array( ID2, TAG2, COUNT2 ), ... )
   * Output: array( TAG1, TAG2, ... )
   *
   * @param  array $tags
   * @return array
   */
  public static function reformatTags($tags) {
    uksort($tags, function ($a, $b) { return $a['count'] - $b['count']; });

    array_walk($tags, function (&$tag) {
      $tag = array(
        'tag' => $tag['tag'],
        'count' => $tag['count'],
      );
    });

    return $tags;
  }

  /**
   * Re-formats comments.
   *
   * @param  array $comments
   * @return array
   */
  public static function reformatComments($comments) {
    /*
    array_walk($comments, function (&$c) {
      $c = array(
        'username' => $c['user_name'],
        'title' => $c['subject'],
        'content' => $c['comment'],
      );
    });
    */

    if (!empty($comments)) {
      // Temporary workarround for strange API data structure
      // (all comments in one dataset separated by <br> tag and with no username and datetime information)
      if (strpos($comments[0], '<br>') !== false) {
        $comments = explode('<br>', $comments[0]);
      }

      array_walk($comments, function (&$c) {
        $c = array(
          'content' => $c,
        );
      });
    }

    return $comments;
  }

  /**
   * Returns the service's identifier for this result item.
   *
   * @return string
   */
  public function getServiceId() {
    return isset($this->raw['nid']) ? $this->raw['nid'] : FALSE;
  }

  /**
   * Returns the resource location/URI as string.
   *
   * @return string
   *   Resource URL.
   */
  public function getResourceLocation() {
    return $this->getData('resource');
  }
}
