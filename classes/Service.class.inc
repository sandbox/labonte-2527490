<?php
/**
 * @file
 * Edutags Service class implementation.
 */

namespace publicplan\wss\edutags;

use publicplan\wss\base\SearchService;

class Service extends SearchService {
  const NAME = 'edutags';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'wss_edutags_settings__search';
  const SETTING__DOMAIN = 'wss_edutags_search_settings__domain';
  const SETTING__ACCESS_TOKEN = 'wss_edutags_settings__access_token';
  const SETTING__RESOURCE_PATH = 'wss_edutags_settings__resource_path';
  const SETTING__ACTIONS_PATH = 'wss_edutags_settings__actions_path';
  const SETTING__MAX_TAG_COUNT = 'wss_edutags_settings__max_tag_count';

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @return array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => null,
      self::SETTING__DOMAIN => 'http://www.edutags.de',
      self::SETTING__ACCESS_TOKEN => null,
      self::SETTING__RESOURCE_PATH => '/api/v1/%KEY/get_resource_by_url?urls=%ID',
      self::SETTING__ACTIONS_PATH => '/export/bookmarks_export/',
      self::SETTING__MAX_TAG_COUNT => 10,
    );
  }

  /**
   * Shorthand method for (bulk) requesting Edutags data.
   *
   * @param  array $urls An array of unique resource URLs.
   * @return \publicplan\wss\edutags\Response
   */
  public static function getMultipleByUrl($urls) {
    if (!self::getSetting(self::SETTING__ACTIVATED)) {
      throw new \RuntimeException(t('Edutags are currently deactivated.'));
    } //if

    $response = Request::get()->setResourceUrls($urls)->send()->getResponse();
    return $response;
  }
}
