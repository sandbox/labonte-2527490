<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use publicplan\wss\edutags\Service;

function wss_edutags_admin_settings() {
  $settings = array();

  $settings[Service::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate Edutags'),
    '#default_value' => Service::getSetting(
        Service::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[Service::SETTING__DOMAIN] = array(
    '#type' => 'textfield',
    '#title' => t('Edutags domain'),
    '#description' => t("eg. 'www.edutags.de' or 'www.edutags-dev.de'."),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__DOMAIN),
    '#weight' => 20,
  );

  $settings[Service::SETTING__RESOURCE_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Edutags-API path'),
    '#description' => 'Syntax: http://www.edutags.de/api/v1/%KEY/%ID/get_resource<br/>' .
        t('%KEY will be replaced by the appropriate access token.<br/>') .
        t('%ID will be replaced by the appropriate resource ID.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__RESOURCE_PATH),
    '#weight' => 22,
  );

  $settings[Service::SETTING__ACCESS_TOKEN] = array(
    '#type' => 'textfield',
    '#title' => t('Access token (User-ID)'),
    '#description' => t('This is the user ID provided by the Edutags administrator.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__ACCESS_TOKEN),
    '#weight' => 24,
  );

  $settings[Service::SETTING__ACTIONS_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Actions path'),
    '#description' => t("Path (with opening and trailing slashes '/'), eg. '/export/bookmarks_export/'."),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__ACTIONS_PATH),
    '#weight' => 26,
  );

  $settings[Service::SETTING__MAX_TAG_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('Tag limit'),
    '#description' => t('Maximum number of tags to list per result.'),
    '#size' => 2,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__MAX_TAG_COUNT),
    '#rules' => array('numeric', 'length[1,5]'),
    '#weight' => 40,
  );
  
  $settings['#submit'] = array('wss_edutags_admin_settings_submit');
  
  return system_settings_form($settings);
}

/**
 * Implements a custom submit handler.
 */
function wss_edutags_admin_settings_submit($form, &$form_state) {
  if (isset($form_state['values']) && !empty($form_state['values'])) {
    cache_clear_all();
    drupal_set_message('Cleared all caches.');
  }
}
